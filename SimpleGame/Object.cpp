#include "stdafx.h"
#include "Object.h"
#include "Globals.h"
#include <math.h>
#include "ScnMgr.h"


Object::Object()
{
	InitPhysics();
}


Object::~Object()
{
}

bool Object::CanShootBullet() {
	if (m_remainingCoolTime < FLT_EPSILON)
		return true;
	return false;
}

void Object::ResetBulletCoolTime(){
	m_remainingCoolTime = m_currentCoolTime;
}

void Object::getVol(float *x, float *y, float *z) {
	*x = m_volX;
	*y = m_volY;
	*z = m_volZ;
}
void Object::setVol(float x, float y, float z) {
	m_volX = x;
	m_volY = y;
	m_volZ = z;
}
void Object::setPos(float x, float y, float z) {
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}
void Object::getPos(float *x, float *y, float *z) {
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}
void Object::setColor(float r, float g, float b, float a){
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}
void Object::getColor(float *r, float *g, float *b, float *a){
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}
void Object::setAcc(float x, float y, float z) {
	m_accX = x;
	m_accY = y;
	m_accZ = z;
}
void Object::getAcc(float *x, float *y, float *z) {
	*x = m_accX;
	*y = m_accY;
	*z = m_accZ;
}
void Object::setVel(float x, float y, float z) {
	m_velX = x;
	m_velY = y;
	m_velZ = z;
}
void Object::getVel(float *x, float *y, float *z) {
	*x = m_velX;
	*y = m_velY;
	*z = m_velZ;
}
void Object::setMass(float mass) {
	m_mass = mass;
}
void Object::getMass(float *mass) {
	*mass = m_mass;
}
void Object::getFricCoef(float *coef){
	*coef = m_fricCoef;
}
void Object::setFricCoef(float coef){
	m_fricCoef = coef;
}
void Object::Update(float elapsedInSec) {
	//reduce remaining cooltime
	m_remainingCoolTime -= elapsedInSec;

	//Apply friction
	float nForce = m_mass * GRAVITY; //scalar, 수직항력
	float fForce = m_fricCoef * nForce; //scalar, 마찰력
	float velSize = sqrtf(m_velX*m_velX + m_velY * m_velY + m_velZ * m_velZ);

	if (velSize > 0.f) {
		float fDirX = -1.f * m_velX / velSize;
		float fDirY = -1.f * m_velY / velSize;
		fDirX = fDirX * fForce;
		fDirY = fDirY * fForce;
		float fAccX = fDirX / m_mass;
		float fAccY = fDirY / m_mass;
		float newX = m_velX + fAccX * elapsedInSec;
		float newY = m_velY + fAccY * elapsedInSec;



		if (newX * m_velX < 0.f)
			m_velX = 0.f;
		else
			m_velX = newX;

		if (newY * m_velY < 0.f)
			m_velY = 0.f;
		else
			m_velY = newY;
		m_velZ = m_velZ - GRAVITY * elapsedInSec;
	}

	m_posX = m_posX + m_velX * elapsedInSec;
	m_posY = m_posY + m_velY * elapsedInSec;
	m_posZ = m_posZ + m_velZ * elapsedInSec;

	if (m_posZ < FLT_EPSILON) {
		m_posZ = 0.f;
		m_velZ = 0.f;
	}
}

void Object::InitPhysics() {
	m_posX = 0.f, m_posY = 0.f, m_posZ = 0.f;//position
	m_volX = 0.f, m_volY = 0.f, m_volZ = 0.f;//volume
	m_velX = 0.f, m_velY = 0.f, m_velZ = 0.f; //velocity
	m_accX = 0.f, m_accY = 0.f, m_accZ = 0.f; //acceleration
	m_r = 0.f, m_g = 0.f, m_b = 0.f, m_a = 0.f; //color
	m_mass = 0.f; //mass
	m_fricCoef = 0.f;
}

void Object::AddForce(float x, float y, float z, float eTime) {
	float accX, accY, accZ;
	accX = accY = accZ = 0;

	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX = m_velX + accX * eTime;
	m_velY = m_velY + accY * eTime;
	m_velZ = m_velZ + accZ * eTime;

}

void Object::setType(int type){
	m_type = type;
}

void Object::getType(int *type){
	*type = m_type;
}

void Object::setTexID(int id) {
	m_texID = id;
}

void Object::setParentObj(Object * parent)
{
	m_parent = parent;
}

Object* Object::getParentObj() {
	return m_parent;
}

bool Object::isAncester(Object* obj){
	
	if (obj != NULL) {
		if (obj == m_parent)
			return true;
	}

	return false;
}

void Object::getTexID(int *id) {
	*id = m_texID;
}

void Object::getHP(float *hp) {
	*hp = m_hp;
}
void Object::setHP(float hp) {
	m_hp = hp;
}
void Object::Damage(float damage) {
	m_hp = m_hp - damage;
}