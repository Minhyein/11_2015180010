#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"

int g_testTex = -1;
int gameOverTex = -1;
int startTex = -1;
int g_state = STATE_START;
int g_BGM = -1;
int g_Fire = -1;
int g_Crush = -1;

int g_BGTecture = -1;
int g_particle = -1;

int back1_x = 0;
int back2_x = 800;

int fire = -1;

float fire_x = 5.f;
float fire2_x = 70.f;
float fire3_x = 49.f;
float fire4_x = 20.f;

int temp, temp2, temp3, temp4;

ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(800, 401);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	//Initialize Physics
	m_Physics = new Physics();
	m_Sound = new Sound();

	// Initialize Object
	for (int i = 0; i < MAX_OBJ_COUNT; i++) {
		m_Obj[i] = NULL;
	}

	//Create Hero
	m_Obj[HERO_ID] = new Object();
	m_Obj[HERO_ID]->setPos(-3, -1.5, 0);
	m_Obj[HERO_ID]->setVol(1, 1, 0.5);
	m_Obj[HERO_ID]->setColor(1, 1, 1, 1);
	m_Obj[HERO_ID]->setVel(0, 0, 0);
	m_Obj[HERO_ID]->setMass(1);
	m_Obj[HERO_ID]->setFricCoef(0.7f);
	m_Obj[HERO_ID]->setHP(500);
	m_Obj[HERO_ID]->setType(TYPE_NORMAL);

	g_testTex = m_Renderer->GenPngTexture("./animation.png");
	g_BGTecture = m_Renderer->GenPngTexture("./background.png");
	m_Obj[HERO_ID]->setTexID(g_testTex);

	temp = AddObj(
		fire_x, 1, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		-0.1f, 0, 0,
		1,
		0.6,
		10,
		TYPE_NORMAL
	);
	temp2 = AddObj(
		fire2_x, -0.5, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		-0.5f, 0, 0,
		1,
		0.6,
		10,
		TYPE_NORMAL
	);
	temp3 = AddObj(
		fire3_x, 0.5, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		-0.3f, 0, 0,
		1,
		0.6,
		10,
		TYPE_NORMAL
	);
	temp4 = AddObj(
		fire4_x, -1.5, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		-0.7f, 0, 0,
		1,
		0.6,
		10,
		TYPE_NORMAL
	);

	g_BGM = m_Sound->CreateBGSound("./Sounds/bgm.mp3");
	m_Sound->PlayBGSound(g_BGM, true, 1.f);
	g_Crush = m_Sound->CreateShortSound("./Sounds/crush.mp3");
	fire = m_Renderer->GenPngTexture("./fireball.png");
	gameOverTex = m_Renderer->GenPngTexture("./gameover.png");
	startTex = m_Renderer->GenPngTexture("./starting.png");
	m_Obj[temp]->setTexID(fire);
	m_Obj[temp2]->setTexID(fire);
	m_Obj[temp3]->setTexID(fire);
	m_Obj[temp4]->setTexID(fire);

}


ScnMgr::~ScnMgr()
{
	if (m_Renderer != NULL) {
		delete m_Renderer;
		m_Renderer = NULL;
	}

	if (m_Physics != NULL) {
		delete m_Physics;
		m_Physics = NULL;
	}
}

void ScnMgr::Update(float elapsedInsec) {
	//Character control : Hero

	if (g_state == STATE_PLAYING) {
		float fx, fy, fz;
		fx = fy = fz = 0.f;
		float fAmount = 10.f; //  1.f=1N

		if (m_KeyW) {
			fy += 1.f;
		}
		if (m_KeyS) {
			fy -= 1.f;
		}
		if (m_KeyD) {
			fx += 1.f;
		}
		if (m_KeyA) {
			fx -= 1.f;
		}
		if (m_KeySpace) {
			fz += 1.f;
		}

		//Add control force to hero
		float fsize = sqrtf(fx*fx + fy * fy);

		if (fsize > FLT_EPSILON) {
			fx /= fsize;
			fy /= fsize;
			fx *= fAmount;
			fy *= fAmount;

			m_Obj[HERO_ID]->AddForce(fx, fy, 0, elapsedInsec);
		}

		if (fz > FLT_EPSILON) {
			float x, y, z;
			m_Obj[HERO_ID]->getPos(&x, &y, &z);

			if (z < FLT_EPSILON) {

				fz += fAmount * 40.f;
				m_Obj[HERO_ID]->AddForce(0, 0, fz, elapsedInsec);

			}

		}


		float x, y, z;
		m_Obj[HERO_ID]->getPos(&x, &y, &z);
		if (x < -3)
			m_Obj[HERO_ID]->setPos(-3, y, z);
		else if (x > 3)
			m_Obj[HERO_ID]->setPos(3, y, z);

		if (y < -1.5)
			m_Obj[HERO_ID]->setPos(x, -1.5, z);
		else if (y > 1.5)
			m_Obj[HERO_ID]->setPos(x, 1.5, z);

		//Fire bullets
	/*	if (m_KeyUp || m_KeyDown || m_KeyLeft || m_KeyRight) {
			float bulletvel = 5.f;
			float vBulletX, vBulletY, vBulletZ;

			vBulletX = vBulletY = vBulletZ = 0.f;

			if (m_KeyUp)vBulletY += 1.f;
			if (m_KeyDown) vBulletY -= 1.f;
			if (m_KeyLeft) vBulletX -= 1.f;
			if (m_KeyRight) vBulletX += 1.f;

			float vBulletSize = sqrtf(vBulletX * vBulletX + vBulletY * vBulletY + vBulletZ * vBulletZ);

			if (vBulletSize > FLT_EPSILON) {
				//Create Bullet Object
				vBulletX /= vBulletSize;
				vBulletY /= vBulletSize;
				vBulletZ /= vBulletSize;

				vBulletX *= bulletvel;
				vBulletY *= bulletvel;
				vBulletZ *= bulletvel;

				float hx, hy, hz;
				float hvx, hvy, hvz;

				m_Obj[HERO_ID]->getPos(&hx, &hy, &hz);
				m_Obj[HERO_ID]->getVel(&hvx, &hvy, &hvz);

				vBulletX += hvx;
				vBulletY += hvy;
				vBulletZ += hvz;

				int id = AddObj(
					hx, hy, hz,
					0.1f, 0.1f, 0.1f,
					1, 0, 0, 1,
					vBulletX, vBulletY, vBulletZ,
					0.1f,
					0.1f,
					25,
					TYPE_BULLET);

				m_Obj[id]->AddForce(vBulletX, vBulletY, vBulletZ, elapsedInsec);
				m_Obj[id]->setParentObj(m_Obj[HERO_ID]);
				m_Obj[HERO_ID]->ResetBulletCoolTime();
				m_Sound->PlayShortSound(g_Fire, false, 1.f);
			}
		}*/

		//Overlaptest
		for (int src = 0; src < MAX_OBJ_COUNT; src++) {
			for (int dst = src + 1; dst < MAX_OBJ_COUNT; dst++) {
				if (m_Obj[src] != NULL && m_Obj[dst] != NULL) {
					if (m_Physics->IsOverlapTest(m_Obj[src], m_Obj[dst])) {
						//std::cout << "Collision : (" << src << ", " << dst << " )" << std::endl;

						if (!m_Obj[src]->isAncester(m_Obj[dst]) && !m_Obj[dst]->isAncester(m_Obj[src])) {
							//Process Collision
							m_Physics->ProcessCollision(m_Obj[src], m_Obj[dst]);

							if (src == HERO_ID || dst == HERO_ID) {
								//Damage
								float srcHP, dstHP;
								m_Obj[dst]->getHP(&dstHP);
								m_Obj[src]->Damage(dstHP);

							}

						}
						m_Sound->PlayShortSound(g_Crush, false, 1.f);
					}
				}
			}
		}



		//for all objs, call update func
		for (int i = 0; i < MAX_OBJ_COUNT; i++) {
			if (m_Obj[i] != NULL)
				m_Obj[i]->Update(elapsedInsec);

		}
	}
}

void ScnMgr::RenderScene() {
	if (g_state == STATE_START) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			800.f, 401.f, 0.f,
			1, 1, 1, 1,
			startTex,
			1.f
		);
		if (m_KeySpace)
			g_state = STATE_PLAYING;
	}
	else if (g_state == STATE_PLAYING) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		//draw background first


		m_Renderer->DrawGround(
			back1_x, 0.f, 0.f,
			800.f, 401.f, 0.f,
			1, 1, 1, 1,
			g_BGTecture,
			1.f
		);
		m_Renderer->DrawGround(
			back2_x, 0.f, 0.f,
			800.f, 401.f, 0.f,
			1, 1, 1, 1,
			g_BGTecture,
			1.f
		);

		back1_x -= 1;
		back2_x -= 1;

		if (back1_x == -800)
			back1_x = 800;
		if (back2_x == -800)
			back2_x = 800;

		fire_x -= 0.05f;
		if (fire_x < -5)
			fire_x = 5;
		fire2_x -= 0.08f;
		if (fire2_x < -7)
			fire2_x = 7;
		fire3_x -= 0.04f;
		if (fire3_x < -9)
			fire3_x = 9;
		fire4_x -= 0.07f;
		if (fire4_x < -5.7)
			fire4_x = 5.7;

		if (m_Obj[temp] != NULL)
			m_Obj[temp]->setPos(fire_x, 1, 0);
		if (m_Obj[temp2] != NULL)
			m_Obj[temp2]->setPos(fire2_x, -0.5, 0);
		if (m_Obj[temp3] != NULL)
			m_Obj[temp3]->setPos(fire3_x, 0.5, 0);
		if (m_Obj[temp4] != NULL)
			m_Obj[temp4]->setPos(fire4_x, -1.5, 0);


		// Draw all m_objs

		for (int i = 0; i < MAX_OBJ_COUNT; i++) {
			if (m_Obj[i] != NULL) {
				float x, y, z = 0;
				float sx, sy, sz = 0;
				float r, g, b, a = 0;

				m_Obj[i]->getPos(&x, &y, &z);
				m_Obj[i]->getVol(&sx, &sy, &sz);
				m_Obj[i]->getColor(&r, &g, &b, &a);

				//1m = 100cm = 100px
				x = x * 100.f;
				y = y * 100.f;
				z = z * 100.f;
				sx = sx * 100.f;
				sy = sy * 100.f;
				sz = sz * 100.f;

				//m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a);
				int texID = -1;
				m_Obj[i]->getTexID(&texID);

				int tempType;
				m_Obj[i]->getType(&tempType);
				float en_hp = 50;
				float temp_hp, hero_hp;
				m_Obj[i]->getHP(&temp_hp);
				m_Obj[HERO_ID]->getHP(&hero_hp);

				if (tempType == TYPE_NORMAL) {
					if (i == HERO_ID) {
						m_Renderer->DrawSolidRectGauge(
							x, y, z,
							0, sy / 2.f, 0,
							sx, 5, 0,
							1, 0, 0, 1,
							hero_hp / 500 * 100);
					}
				}


				if (i == HERO_ID) {
					static int temp = 0;
					int ix = temp % 8;
					int iy = (int)((float)temp / (float)8);
					m_Renderer->DrawTextureRectAnim(
						x, y, z,
						sx, sy, sz,
						r, g, b, a,
						texID,
						8,
						4,
						ix,
						iy);
					temp++;
					if (temp > 65)
						temp = 0;
				}
				else {
					m_Renderer->DrawTextureRect(
						x, y, z,
						sx, sy, sz,
						r, g, b, a,
						texID);
				}
			}
		}

	}
	
	else if (g_state == STATE_GAMEOVER) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		//draw background first


		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			800.f, 401.f, 0.f,
			1, 1, 1, 1,
			gameOverTex,
			1.f
		);
		if (m_KeySpace)
			exit(0);
	}
	
}

void ScnMgr::DoGarbageCollection() {
	for (int i = 0; i < MAX_OBJ_COUNT; i++) {
		if (m_Obj[i] == NULL)
			continue;

		int type = -1;
		m_Obj[i]->getType(&type);

		if (type == TYPE_BULLET) {
			//Check Velocity size
			float vx, vy, vz;
			m_Obj[i]->getVel(&vx, &vy, &vz);
			float vSize = sqrtf(vx*vx + vy * vy + vz * vz);

			if (vSize < FLT_EPSILON) {
				DeleteObj(i);
				continue;
			}
		}

		float hp;
		m_Obj[i]->getHP(&hp);
		if (hp < FLT_EPSILON) {
			if (i != HERO_ID) {
				DeleteObj(i);
				continue;
			}
			else {
				DeleteObj(i);
				g_state = STATE_GAMEOVER;
				return;
			}
		}

	}
}

int ScnMgr::AddObj(float x, float y, float z, float sx, float sy, float sz, float r, float g, float b, float a, float vx, float vy, float vz,
	float mass, float fricCoef, float hp, int type) {

	int idx = -1;
	for (int i = 0; i < MAX_OBJ_COUNT; i++) {
		if (m_Obj[i] == NULL) {
			idx = i;
			break;
		}
	}

	if (idx == -1) {
		std::cout << "No more empty object slot." << std::endl;
		return -1;
	}

	m_Obj[idx] = new Object();
	m_Obj[idx]->setPos(x, y, z);
	m_Obj[idx]->setVol(sx, sy, sz);
	m_Obj[idx]->setColor(r, g, b, a);
	m_Obj[idx]->setVel(vx, vy, vz);
	m_Obj[idx]->setMass(mass);
	m_Obj[idx]->setFricCoef(fricCoef);
	m_Obj[idx]->setType(type);
	m_Obj[idx]->setHP(hp);


	return idx;
}

void ScnMgr::DeleteObj(int idx) {

	if (idx < 0) {
		std::cout << "input idx is negative : " << idx << std::endl;
	}
	if (idx >= MAX_OBJ_COUNT) {
		std::cout << "input idx exceeds MAX_OBJ_COUNT : " << idx << std::endl;
	}
	if (m_Obj[idx] == NULL) {
		std::cout << "m_Obj[" << idx << "] is NULL" << std::endl;
	}

	delete m_Obj[idx];
	m_Obj[idx] = NULL;

}

void ScnMgr::KeyDownInput(unsigned char key, int x, int y) {
	if (key == 'w' || key == 'W') {
		m_KeyW = true;
	}
	if (key == 'a' || key == 'A') {
		m_KeyA = true;
	}
	if (key == 's' || key == 'S') {
		m_KeyS = true;
	}
	if (key == 'd' || key == 'D') {
		m_KeyD = true;
	}
	if (key == ' ') {
		m_KeySpace = true;
	}
}
void ScnMgr::KeyUpInput(unsigned char key, int x, int y) {
	if (key == 'w' || key == 'W') {
		m_KeyW = false;
	}
	if (key == 'a' || key == 'A') {
		m_KeyA = false;
	}
	if (key == 's' || key == 'S') {
		m_KeyS = false;
	}
	if (key == 'd' || key == 'D') {
		m_KeyD = false;
	}
	if (key == ' ') {
		m_KeySpace = false;
	}
}

void ScnMgr::SpecialKeyDownInput(int key, int x, int y) {
	if (key == GLUT_KEY_UP) {
		m_KeyUp = true;
	}
	if (key == GLUT_KEY_LEFT) {
		m_KeyLeft = true;
	}
	if (key == GLUT_KEY_DOWN) {
		m_KeyDown = true;
	}
	if (key == GLUT_KEY_RIGHT) {
		m_KeyRight = true;
	}
}

void ScnMgr::SpecialKeyUpInput(int key, int x, int y) {
	if (key == GLUT_KEY_UP) {
		m_KeyUp = false;
	}
	if (key == GLUT_KEY_LEFT) {
		m_KeyLeft = false;
	}
	if (key == GLUT_KEY_DOWN) {
		m_KeyDown = false;
	}
	if (key == GLUT_KEY_RIGHT) {
		m_KeyRight = false;
	}
}

