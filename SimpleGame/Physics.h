#pragma once
#include "Object.h"

class Physics
{
public:
	Physics();
	~Physics();

	bool IsOverlapTest(Object *A, Object *B, int method = 0);
	void ProcessCollision(Object *A, Object *B);

private:
	bool BBOverlapTest(Object *A, Object *B);
	
};

