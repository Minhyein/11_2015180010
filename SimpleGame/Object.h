#pragma once
class Object
{
public:
	Object();
	~Object();

	void getPos(float *x, float *y, float *z);
	void getMass(float *mass);
	void getAcc(float *x, float *y, float *z);
	void getVel(float *x, float *y, float *z);
	void getVol(float *x, float *y, float *z);
	void getColor(float *r, float *g, float *b, float *a);
	void getFricCoef(float *coef);
	void getType(int *type);
	void getTexID(int *id);

	void setPos(float x, float y, float z);
	void setMass(float mass);
	void setAcc(float x, float y, float z);
	void setVel(float x, float y, float z);
	void setVol(float x, float y, float z);
	void setColor(float r, float g, float b, float a);
	void setFricCoef(float coef);
	void setType(int type);
	void setTexID(int id);
	void getHP(float *hp);
	void setHP(float hp);
	void Damage(float damage);

	void setParentObj(Object* parent);
	Object* getParentObj();
	
	bool isAncester(Object *obj);

	void Update(float elpasedInSec);
	void InitPhysics();
	void AddForce(float x, float y, float z, float eTime);

	bool CanShootBullet();
	void ResetBulletCoolTime();

private:
	float m_posX, m_posY, m_posZ;//position
	float m_volX, m_volY, m_volZ;//volume
	float m_velX, m_velY, m_velZ; //velocity
	float m_accX, m_accY, m_accZ; //acceleration
	float m_r, m_g, m_b, m_a; //color
	float m_mass; //mass
	float m_fricCoef; //�������
	int m_type; //object type
	float m_remainingCoolTime = 0.f;
	float m_currentCoolTime = 0.f;
	int m_texID; //texID
	float m_hp;

	Object *m_parent = NULL;
	
};

