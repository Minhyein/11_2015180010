#pragma once

#include "Renderer.h"
#include "Object.h"
#include "Globals.h"
#include "Physics.h"
#include "Sound.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void RenderScene();
	int AddObj(float x, float y, float z,
		float sx, float sy, float sz,
		float r, float g, float b, float a,
		float vx, float vy, float vz,
		float mass, 
		float fricCoef, 
		float helthpoint,
		int type);

	void DeleteObj(int idx);
	void Update(float elapsedTime);
	void DoGarbageCollection();

	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(int key, int x, int y);
	void SpecialKeyUpInput(int key, int x, int y);

	

private:
	Renderer * m_Renderer = NULL;
	Sound * m_Sound = NULL;
	Physics *m_Physics = NULL;
	Object * m_TestObj = NULL;
	Object * m_Obj[MAX_OBJ_COUNT];
	Object * m_Mon[10];
	int m_TestChar[10];
	
	bool m_KeyW = false;
	bool m_KeyA = false;
	bool m_KeyS = false;
	bool m_KeyD = false;
	bool m_KeySpace = false;

	bool m_KeyUp = false;
	bool m_KeyLeft = false;
	bool m_KeyDown = false;
	bool m_KeyRight = false;
};

