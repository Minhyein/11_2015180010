#pragma once

#define MAX_OBJ_COUNT 100

#define HERO_ID 0

#define GRAVITY 9.8f

#define TYPE_NORMAL 0
#define TYPE_BULLET 1

#define STATE_PLAYING 0
#define STATE_GAMEOVER 1
#define STATE_START -1
