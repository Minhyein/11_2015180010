#include "stdafx.h"
#include "Physics.h"


Physics::Physics()
{
}


Physics::~Physics()
{
}

bool Physics::IsOverlapTest(Object *A, Object *B, int method) {

	switch (method) {
	case 0:	//BB overlap test
		return BBOverlapTest(A, B);
		break;
	default:
		break;
	}

	return false;
}


bool Physics::BBOverlapTest(Object *A, Object *B) {

	//A object 
	float aX, aY, aZ;
	float aSX, aSY, aSZ;
	float aminX, aminY, aminZ;
	float amaxX, amaxY, amaxZ;

	A->getPos(&aX, &aY, &aZ);
	A->getVol(&aSX, &aSY, &aSZ);

	aminX = aX - aSX / 2.f;
	aminY = aY - aSY / 2.f;
	aminZ = aZ - aSZ / 2.f;

	amaxX = aX + aSX / 2.f;
	amaxY = aY + aSY / 2.f;
	amaxZ = aZ + aSZ / 2.f;

	//B object
	float bX, bY, bZ;
	float bSX, bSY, bSZ;
	float bminX, bminY, bminZ;
	float bmaxX, bmaxY, bmaxZ;

	B->getPos(&bX, &bY, &bZ);
	B->getVol(&bSX, &bSY, &bSZ);

	bminX = bX - bSX / 2.f;
	bminY = bY - bSY / 2.f;
	bminZ = bZ - bSZ / 2.f;

	bmaxX = bX + bSX / 2.f;
	bmaxY = bY + bSY / 2.f;
	bmaxZ = bZ + bSZ / 2.f;

	if (aminX > bmaxX)
		return false;
	if (amaxX < bminX)
		return false;

	if (aminY > bmaxY)
		return false;
	if (amaxY < bminY)
		return false;

	if (aminZ > bmaxZ)
		return false;
	if (amaxZ < bminZ)
		return false;

	return true;
}

void Physics::ProcessCollision(Object *A, Object *B){
	//AObect 
	float aMass;
	float aVX, aVY, aVZ;
	float afVX, afVY, afVZ; //finish veloity

	A->getMass(&aMass);
	A->getVel(&aVX, &aVY, &aVZ);

	
	//BObject
	float bMass;
	float bVX, bVY, bVZ;
	float bfVX, bfVY, bfVZ; //finish veloity

	B->getMass(&bMass);
	B->getVel(&bVX, &bVY, &bVZ);

	afVX = ((aMass - bMass) / (aMass + bMass)) * aVX + ((2.f * bMass) / (aMass + bMass))*bVX;
	afVY = ((aMass - bMass) / (aMass + bMass)) * aVY + ((2.f * bMass) / (aMass + bMass))*bVY;
	afVZ = ((aMass - bMass) / (aMass + bMass)) * aVZ + ((2.f * bMass) / (aMass + bMass))*bVZ;

	bfVX = ((2.f * aMass) / (aMass + bMass)) * aVX + ((bMass - aMass) / (aMass + bMass)) * bVX;
	bfVY = ((2.f * aMass) / (aMass + bMass)) * aVY + ((bMass - aMass) / (aMass + bMass)) * bVY;
	bfVZ = ((2.f * aMass) / (aMass + bMass)) * aVZ + ((bMass - aMass) / (aMass + bMass)) * bVZ;

	A->setVel(afVX, afVY, afVZ);
	B->setVel(bfVX, bfVY, bfVZ);
}